#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>
#include <geometric_shapes/shape_operations.h>
#include <vector>
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <tuple>
#include <tf_conversions/tf_eigen.h>
#define _USE_MATH_DEFINES
#include "tf/transform_datatypes.h"
#include <cmath>




// Define listener as global pointer.
boost::shared_ptr<tf::TransformListener> listener;

boost::shared_ptr<moveit::planning_interface::MoveGroupInterface> move_group;
boost::shared_ptr<moveit::planning_interface::MoveGroupInterface> move_grasp_group;


Eigen::Matrix4d Rodrigues(double roll, double pitch, double yaw){
  // Compare the results with the folling webpage 
  // https://www.andre-gaschler.com/rotationconverter/
  std::cout << "We are inside Rodrigues " << std::endl;
  tf::Matrix3x3 R_init;
  R_init.setRPY(roll,pitch,yaw);
  Eigen::Matrix3d Rotation_Matrix;
  tf::matrixTFToEigen(R_init,Rotation_Matrix);
  Eigen::Matrix4d homogeneousTF;
  homogeneousTF.setIdentity();
  homogeneousTF.block (0, 0, 3, 3) = Rotation_Matrix;
  std::cout << homogeneousTF<< std::endl;
  return homogeneousTF;
}

Eigen::Matrix4d Rx(double angle){
  std::cout << "We are inside Rx " << std::endl;
  return Rodrigues(angle,0,0);
}

Eigen::Matrix4d Ry(double angle){
  std::cout << "We are inside Ry " << std::endl;
  return Rodrigues(0,angle,0);
}

Eigen::Matrix4d Rz(double angle){
  std::cout << "We are inside Rz " << std::endl;
  return Rodrigues(0,0,angle);
}



Eigen::Matrix4d get_transformation_matrix(tf::Vector3 position,tf::Quaternion orientation ){
  // Given position and quartenion, get TF 4x4 Matrix
  std::cout <<  "get_transformation_matrix " << std::endl;
  Eigen::Matrix4d tf_eigen;
  tf::Matrix3x3 orientatin_matrix(orientation);
    Eigen::Matrix3d R;
  tf::matrixTFToEigen(orientatin_matrix,R);
  std::string sep = "\n----------------------------------------\n";
  std::cout << R << sep;
    // Set
  tf_eigen.setZero ();
  tf_eigen.block (0, 0, 3, 3) = R;
  Eigen::Vector3d P(
          position.x(),
          position.y(),
          position.z()
          );
  tf_eigen.block (0, 3, 3, 1) = P;
  tf_eigen (3, 3) = 1;
  std::cout << "TF Eigen " <<  std::endl;
  std::cout << tf_eigen << sep;
  return tf_eigen;

}

std::tuple<tf::Vector3, tf::Quaternion> get_pos_and_rot_between_frames(std::string base_frame, std::string target_frame){
  std::cout <<  "base_frame " << base_frame << std::endl;
  std::cout <<  "target_frame " << target_frame << std::endl;
  tf::StampedTransform transform;
  listener->lookupTransform(base_frame,target_frame,ros::Time(0), transform);
  tf::Vector3 position = transform.getOrigin();
  tf::Quaternion orientation = transform.getRotation();
  // get_transformation_matrix(position,orientation);

  return  std::make_tuple(position, orientation);

}

void move_to_position(std::vector<double> pos, std::vector<double> rot){
  geometry_msgs::Pose target_pose;
  target_pose.orientation.x = rot.at(0);
  target_pose.orientation.y = rot.at(1);
  target_pose.orientation.z = rot.at(2);
  target_pose.orientation.w = rot.at(3);
  target_pose.position.x = pos.at(0);
  target_pose.position.y = pos.at(1);
  target_pose.position.z = pos.at(2);
  move_group->setPoseTarget(target_pose);
  move_group->move();
  std::cout << "Execution is done" << std::endl;
  move_group->stop();
  move_group->clearPoseTargets();
  std::cout << "Current Pose: " << std::endl;
  std::cout << move_group->getCurrentPose() << std::endl;

}



void go_to_pose_goal(const tf::Vector3 &pos_tf, const tf::Quaternion &rot_tf){

 
        std::cout << "X: " << pos_tf.x() << std::endl;
        std::cout << "Y: " << pos_tf.y() << std::endl;
        std::cout << "Z: " << pos_tf.z() << std::endl;

        std::cout << "rotx : " << rot_tf.x() << std::endl;
        std::cout << "roty : " << rot_tf.y() << std::endl;
        std::cout << "rotz : " << rot_tf.z() << std::endl;
        std::cout << "rotw : " << rot_tf.w() << std::endl;

        geometry_msgs::Pose target_pose;
        target_pose.orientation.x = rot_tf.x();
        target_pose.orientation.y = rot_tf.y();
        target_pose.orientation.z = rot_tf.z();
        target_pose.orientation.w = rot_tf.w();
        target_pose.position.x = pos_tf.x();
        target_pose.position.y = pos_tf.y();
        target_pose.position.z = pos_tf.z();
        move_group->setPoseTarget(target_pose);
        move_group->move();
        std::cout << "Execution is done" << std::endl;
        move_group->stop();
        move_group->clearPoseTargets();
        std::cout << "Current Pose: " << std::endl;
        std::cout << move_group->getCurrentPose() << std::endl;

}


void go_to_frame(std::string name_of_frame, std::vector<double> tolerance ={0,0,0}){
  
    tf::Vector3 loc;
    tf::Quaternion rot;
    std::tie(loc, rot) = get_pos_and_rot_between_frames("world",name_of_frame);
    Eigen::Matrix4d TF_token_to_world = get_transformation_matrix(loc,rot);
    Eigen::Matrix4d end_effector_target = TF_token_to_world *  Ry(M_PI);
    
    std::tie(loc, rot) = get_pos_and_rot_between_frames("end_effector_frame","iiwa_link_ee");
    Eigen::Matrix4d TF_iiwa_link_ee_to_end_effector_frame = get_transformation_matrix(loc,rot);
    Eigen::Matrix4d  TF_iiwa_link_ee_to_world = end_effector_target * TF_iiwa_link_ee_to_end_effector_frame;

    std::cout << "go_to_frame" << std::endl;
    std::cout << TF_iiwa_link_ee_to_world << std::endl;

    // Convert Rotation matrix to the quartenion represnentation.
    Eigen::Matrix3d TF_iiwa_link_ee_to_world3x3 = TF_iiwa_link_ee_to_world.block(0, 0, 3, 3);

    Eigen::Quaterniond q(TF_iiwa_link_ee_to_world3x3);
    tf::Quaternion rot_tf;
    tf::quaternionEigenToTF(q,rot_tf);

    Eigen::Vector3d pos = TF_iiwa_link_ee_to_world.block (0, 3, 3, 1);
    tf::Vector3 pos_tf;
    tf::vectorEigenToTF (pos, pos_tf);

    go_to_pose_goal(pos_tf,rot_tf);
 

}


void gripper(moveit::planning_interface::MoveGroupInterface &move_grasp_group, std::string gripper_state){
  std::cout << "Gripper state is:  " << gripper_state << std::endl;
  move_grasp_group.setNamedTarget(gripper_state);
  move_grasp_group.move();
  std::cout << "Gripper execution is completed. Now:  " << gripper_state << std::endl;
  move_grasp_group.stop();
  move_grasp_group.clearPoseTargets();
  std::cout << "Current Pose: " << std::endl;
  std::cout << move_grasp_group.getCurrentPose() << std::endl;



}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "move_group_interface_tutorial");
  ros::NodeHandle node_handle;
  ros::AsyncSpinner spinner(1);
  spinner.start();
  // tf::TransformListener listener;
  listener.reset(new tf::TransformListener());
  ros::Rate rate(10.0);
  tf::StampedTransform transform;
  Eigen::Matrix4f Tm;
  try{
    ros::Time now = ros::Time::now();
    listener->waitForTransform("/world", "/iiwa_link_ee", now, ros::Duration(3.0));
    // tf::Vector3 var1;
    // tf::Quaternion var2;
    // std::tie(var1, var2) = get_pos_and_rot_between_frames("/world","/iiwa_link_ee");
    // std::cout << "outside of the function" << std::endl;
    // std::cout << var1.x() << var2.w() << std::endl;
    // ros::Duration(3.0).sleep();

    

  }
  catch (tf::TransformException &ex) {
    ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
  }



  // BEGIN_TUTORIAL
  //
  // Setup
  // ^^^^^
  //
  // MoveIt operates on sets of joints called "planning groups" and stores them in an object called
  // the `JointModelGroup`. Throughout MoveIt the terms "planning group" and "joint model group"
  // are used interchangably.
  const std::string PLANNING_GROUP = "iiwa_plan_group";

  const std::string GRASP_PLANNING_GROUP = "grasp_plan_group";

  // The :planning_interface:`MoveGroupInterface` class can be easily
  // setup using just the name of the planning group you would like to control and plan for.
  move_group.reset(new moveit::planning_interface::MoveGroupInterface (PLANNING_GROUP));
  move_grasp_group.reset(new moveit::planning_interface::MoveGroupInterface (GRASP_PLANNING_GROUP));

  // moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
  // moveit::planning_interface::MoveGroupInterface move_grasp_group(GRASP_PLANNING_GROUP)

// We will use the :planning_interface:`PlanningSceneInterface`
  // class to add and remove collision objects in our "virtual world" scene
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

  // Raw pointers are frequently used to refer to the planning group for improved performance.
  const moveit::core::JointModelGroup* joint_model_group =
      move_group->getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    

  // gripper(move_grasp_group,"gripper_closed");
  std::vector<double> position_array {0.04,0.6,0.75};
  std::vector<double> rotation_array {0,1,0,0};
  // move_to_position(position_array,rotation_array);

  // Visualization
  // ^^^^^^^^^^^^^
  //
  // The package MoveItVisualTools provides many capabilities for visualizing objects, robots,
  // and trajectories in RViz as well as debugging tools such as step-by-step introspection of a script.
  namespace rvt = rviz_visual_tools;
  moveit_visual_tools::MoveItVisualTools visual_tools("iiwa_link_0");
  visual_tools.deleteAllMarkers();

  // Remote control is an introspection tool that allows users to step through a high level script
  // via buttons and keyboard shortcuts in RViz
  visual_tools.loadRemoteControl();

  // RViz provides many types of markers, in this demo we will use text, cylinders, and spheres
  Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
  text_pose.translation().z() = 1.0;
  visual_tools.publishText(text_pose, "MoveGroupInterface Demo", rvt::WHITE, rvt::XLARGE);

  // Batch publishing is used to reduce the number of messages being sent to RViz for large visualizations
  visual_tools.trigger();

  // Getting Basic Information
  // ^^^^^^^^^^^^^^^^^^^^^^^^^
  //
  // We can print the name of the reference frame for this robot.
  ROS_INFO_NAMED("tutorial", "Planning frame: %s", move_group->getPlanningFrame().c_str());

  // We can also print the name of the end-effector link for this group.
  ROS_INFO_NAMED("tutorial", "End effector link: %s", move_group->getEndEffectorLink().c_str());
    // We can get a list of all the groups in the robot:
  ROS_INFO_NAMED("tutorial", "Available Planning Groups:");
  std::copy(move_group->getJointModelGroupNames().begin(), move_group->getJointModelGroupNames().end(),
            std::ostream_iterator<std::string>(std::cout, ", "));

  // go_to_frame("hanoi_tower_d300_model1_frame");

  robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
  robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
  ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());
  robot_state::RobotStatePtr kinematic_state(new robot_state::RobotState(kinematic_model));


  Eigen::Vector3d reference_point_position(0.0, 0.0, 0.0);
  Eigen::MatrixXd jacobian;
  kinematic_state->getJacobian(joint_model_group,
                             kinematic_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                             reference_point_position, jacobian);
  ROS_INFO_STREAM("Jacobian: \n" << jacobian << "\n");

  Eigen::VectorXd v(6);
	v(0) = 0.0892664;
	v(1) = -0.000622712;
	v(2) = -7.15226;
	v(3) = 2.93941e-05;
	v(4) = 0.0694714;
	v(5) = -0.00021923;

  std::cout << jacobian.transpose() << std::endl;
  std::cout << jacobian.transpose()* v << std::endl;





  // Start the demo
  // ^^^^^^^^^^^^^^^^^^^^^^^^^
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to start the demo");

  // // .. _move_group_interface-planning-to-pose-goal:
  // //
  // // Planning to a Pose goal
  // // ^^^^^^^^^^^^^^^^^^^^^^^
  // // We can plan a motion for this group to a desired pose for the
  // // end-effector.

  
  // geometry_msgs::Pose target_pose1;
  // target_pose1.orientation.x = 0;
  // target_pose1.orientation.y = 1;
  // target_pose1.orientation.z = 0;
  // target_pose1.orientation.w = 0;
  // target_pose1.position.x = 0.040;
  // target_pose1.position.y = 0.60;
  // target_pose1.position.z = 0.75;
  // move_group.setPoseTarget(target_pose1);

  // // Now, we call the planner to compute the plan and visualize it.
  // // Note that we are just planning, not asking move_group
  // // to actually move the robot.
  // moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  // // move_group.move();
  // bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);

  // ROS_INFO_NAMED("tutorial", "Visualizing plan 1 (pose goal) %s", success ? "success" : "FAILED");

  // // Visualizing plans
  // // ^^^^^^^^^^^^^^^^^
  // // We can also visualize the plan as a line with markers in RViz.
  // ROS_INFO_NAMED("tutorial", "Visualizing plan 1 as trajectory line");
  // visual_tools.publishAxisLabeled(target_pose1, "pose1");
  // visual_tools.publishText(text_pose, "Pose Goal", rvt::WHITE, rvt::XLARGE);
  // visual_tools.publishTrajectoryLine(my_plan.trajectory_, joint_model_group);
  // visual_tools.trigger();
  // visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");

  // // Moving to a pose goal
  // // ^^^^^^^^^^^^^^^^^^^^^
  // //
  // // Moving to a pose goal is similar to the step above
  // // except we now use the move() function. Note that
  // // the pose goal we had set earlier is still active
  // // and so the robot will try to move to that goal. We will
  // // not use that function in this tutorial since it is
  // // a blocking function and requires a controller to be active
  // // and report success on execution of a trajectory.

  // /* Uncomment below line when working with a real robot */
  // /* move_group.move(); */

  // // Planning to a joint-space goal
  // // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  // //
  // // Let's set a joint space goal and move towards it.  This will replace the
  // // pose target we set above.
  // //
  // // To start, we'll create an pointer that references the current robot's state.
  // // RobotState is the object that contains all the current position/velocity/acceleration data.
  // moveit::core::RobotStatePtr current_state = move_group.getCurrentState();
  // //
  // // Next get the current set of joint values for the group.
  // std::vector<double> joint_group_positions;
  // current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);

  // // Now, let's modify one of the joints, plan to the new joint space goal and visualize the plan.
  // joint_group_positions[0] = -1.0;  // radians
  // move_group.setJointValueTarget(joint_group_positions);

  // // We lower the allowed maximum velocity and acceleration to 5% of their maximum.
  // // The default values are 10% (0.1).
  // // Set your preferred defaults in the joint_limits.yaml file of your robot's moveit_config
  // // or set explicit factors in your code if you need your robot to move faster.
  // move_group.setMaxVelocityScalingFactor(0.05);
  // move_group.setMaxAccelerationScalingFactor(0.05);

  // success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
  // ROS_INFO_NAMED("tutorial", "Visualizing plan 2 (joint space goal) %s", success ? "success" : "FAILED");


  // // Visualize the plan in RViz
  // visual_tools.deleteAllMarkers();
  // visual_tools.publishText(text_pose, "Joint Space Goal", rvt::WHITE, rvt::XLARGE);
  // visual_tools.publishTrajectoryLine(my_plan.trajectory_, joint_model_group);
  // visual_tools.trigger();
  // visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the Path Constraints");



  // // Adding specific mesh object to the RVIZ
  // moveit_msgs::CollisionObject collision_object;
  // collision_object.header.frame_id = move_group.getPlanningFrame();

  // // The id of the object is used to identify it.
  // collision_object.id = "hanoi_board";
  // shapes::Mesh* m = shapes::createMeshFromResource("package://hanoi_tower_kuka_model/models/hanoi_tower_models/hanoi_board/meshes/hanoi-board.stl"); 
  // shape_msgs::Mesh mesh;
  // shapes::ShapeMsg mesh_msg;  
  // shapes::constructMsgFromShape(m, mesh_msg);
  // mesh = boost::get<shape_msgs::Mesh>(mesh_msg);
  // // Define a pose for the box (specified relative to frame_id)
  // geometry_msgs::Pose hanoi_board_pose;
  // hanoi_board_pose.orientation.w = 1.0;
  // hanoi_board_pose.position.x = 0.5;
  // hanoi_board_pose.position.y = 0.0;
  // hanoi_board_pose.position.z = 0.25;

  // collision_object.meshes.push_back(mesh);
  // collision_object.mesh_poses.push_back(hanoi_board_pose);
  // collision_object.operation = collision_object.ADD;

  // std::vector<moveit_msgs::CollisionObject> collision_objects;
  // collision_objects.push_back(collision_object);

  // // Now, let's add the collision object into the world
  // // (using a vector that could contain additional objects)
  // ROS_INFO_NAMED("hanoi_tower_kuka_model", "Add an object into the world");
  // planning_scene_interface.addCollisionObjects(collision_objects);

  // // Show text in RViz of status and wait for MoveGroup to receive and process the collision object message
  // visual_tools.publishText(text_pose, "Add object", rvt::WHITE, rvt::XLARGE);
  // visual_tools.trigger();
  // visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to once the collision object appears in RViz");


}




