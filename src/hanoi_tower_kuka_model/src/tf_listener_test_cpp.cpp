
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "talker");

  ros::NodeHandle node;


  tf::TransformListener listener;

  ros::Rate rate(10.0);
  while (node.ok()){
    tf::StampedTransform transform;
    try{
        ros::Time now = ros::Time::now();
        listener.waitForTransform("/world", "/iiwa_link_ee", now, ros::Duration(3.0));
        listener.lookupTransform("/world","/iiwa_link_ee",ros::Time(0), transform);
        std::cout << "transform exist\n";

        std::cout << "X: " << transform.getOrigin().x() << std::endl;
        std::cout << "Y: " << transform.getOrigin().y() << std::endl;
        std::cout << "Z: " << transform.getOrigin().z() << std::endl;

        std::cout << "rotx : " << transform.getRotation().x() << std::endl;
        std::cout << "roty : " << transform.getRotation().y() << std::endl;
        std::cout << "rotz : " << transform.getRotation().z() << std::endl;
        std::cout << "rotw : " << transform.getRotation().w() << std::endl;
 
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }

    rate.sleep();
  }
  return 0;
};